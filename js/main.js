//#region Configuración y instanciación del servico AWS 
// Configuración de AWS SDK
AWS.config.update({
    accessKeyId: '',
    secretAccessKey: '',
    region: ''

});
//  configuracion de instancia del servicio
window.s3 = new AWS.S3({
    apiVersion: '2006-03-01',
    params: { Bucket: '' }
});
//#endregion

//#region  Método para lista la data obtenida del servidor 

function listData() {
    /**
     * listObjects retorna la lista de objetos encontrados en el servidor, se pueden usar filtros en este caso 
     * se recuperan los objetos de la carpeta raiz 
     */
   
    window.s3.listObjects({ Delimiter: "/" },, function (err, data) {
        if (err) {
             return console.log(err);
             
        } else {
             
         /**
          * extructura html de los archivos 
          */
      var mainfolder = getHtml([
        "<li>",
        '<div class="row m-auto">',
        '<div class="col-6">',
        '<span aria-hidden="false" class="fa fa-folder-open" id="folder_main"></span>',
        "/",
        "</div>",
        ' <div class="col-3 m-0 p-0 ">',
        "</div>",
        "</li>"
      ]);

               /** 
                * Se listan las carpetas obtenidas del servidor 
               */
               var albums = data.CommonPrefixes.map(function (commonPrefix) {
               var prefix = commonPrefix.Prefix;
               var albumName = decodeURIComponent(prefix.replace('/', ''));

               return getHtml([
                  "<li>",
                  '<div class="row ml-4 px-2">',
                  '<div class="col-6  m-0 p-0 ">',
                  '<span aria-hidden="false" class="fa fa-folder" id="folder_' +
                   albumName +
                  '" ></span>',
                   albumName,"</div>",
                   ' <div class="col-3 m-0 p-0 ">',
                   '<button type="button" class="btn btn-link  btn-sm hover " data-target="' +
                    albumName +'" data-folder-icon="folder_' +albumName +'" id="verfolder_' +
                   albumName +'" ><span class="fa fa-eye " data-toggle="tooltip" data-placement="top" title="abrir carpeta ' +
                   albumName +'"></span></button>',"</div>"," </div>","</li>"
               ]);
             });

             var htmlTemplate = ['<ul class="folder-list m-0 p-0">',
                                 mainfolder, getHtml(albums), "</ul>"];
             document.getElementById('listFiles').innerHTML = getHtml(htmlTemplate);
           }
       });
}
//#endregion

 //#region Método para cargar archivo al servidor
/**
 * 
 * @param {string} albumName 
 */
function addFile (folderName) {
    folderName = folderName.trim();
    var files = document.getElementById("btnUploadFile").files;
    if (!files.length) {
      return alertify.error("Debe seleccionar un archivo");
    }
    var file = files[0];
    var albumName = file.name;
    var folder_key;
    
      folder_key = encodeURIComponent(folderName) + "/";

    var fileKey = folder_key + albumName;
     window.s3.upload(
       {
         Key: fileKey,                 // llave del archivo a subir
         Body: file,                   // archivo a subir
         ACL: "public-read",           // tipo de permiso
         ContentDisposition: "inline", // headres para forzar visualizacion del archivo
         ContentType: "image/jpeg",    // tipo de archivo subido
       },
       function(err, data) {
         if (err) {
           return alert('Error, se a presentado un problema ', err.message);
         }
         alertify.success("Se ha cargado el archivo !! :) ");
         listData();
         viewFolder(folderName);
       }
     );
 }
//#endregion 
