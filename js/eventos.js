(function(){
    $("#newfolder").click(function() {
      _prompt();
    });
    $(document).on("bindevents",function(){
        $('button[id^="verfolder_"]').click(function() {
          $('span[id^="folder_"]').removeClass("fa-folder-open");
          $('span[id^="folder_"]').addClass("fa-folder");
          var x = $(this).attr("data-target");
          var icon = $('#'+$(this).attr("data-folder-icon"));
          icon.removeClass('fa-folder');
          icon.addClass("fa-folder-open");

          viewFolder(x);
        });

    });
     $(document).on("bindevents_uploadfile",function(){

        $("#selectFile").click(function(){
            $("#filesUpload_control").click();
        });
        $("#addFile").click(function() {
          var folderName = $(this).attr("data-folder-name");
          addFile(folderName);
        });
        $("#filesUpload_control").on('change',function(e) {
            
            if (e.target.files.length > 0) {
                $("#filename").text(e.target.files[0].name);
                $("#addFile").css("display", "initial");
            }
        });
    });
})();
